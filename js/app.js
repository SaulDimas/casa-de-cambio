const monedaOrigen = document.getElementById("monedaOrigen");
const monedaDestino = document.getElementById("monedaDestino");

monedaOrigen.addEventListener("change", () => {
  const seleccionado = monedaOrigen.value;
  console.log("Moneda seleccionada en origen: " + seleccionado);
  for (let i = 0; i < monedaDestino.options.length; i++) {
    if (monedaDestino.options[i].value === seleccionado) {
      console.log("Opción a deshabilitar: " + monedaDestino.options[i].value);
      monedaDestino.options[i].disabled = true;
      monedaDestino.selectedIndex = i + 1;
    } else {
      monedaDestino.options[i].disabled = false;
    }
  }
});

